package se.ju.threemusks.savingprivatebudget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditBudgetPostDialog extends AppCompatDialogFragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    final List<String> spinnerList = new ArrayList<>();
    private Context mContext;
    private TextView nameTextView;
    private EditText amountEditText;
    private MaterialBetterSpinner spinner;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        mContext = getContext();
        final String selectedYear = getTag();

        final int index = getArguments().getInt("index");
        String budgetKeyValueString = Data.budgetKeyValueStringList.get(index);
        final String key = budgetKeyValueString.substring(0, (budgetKeyValueString.indexOf(":")-1));
        final String value = budgetKeyValueString.substring((budgetKeyValueString.lastIndexOf(":")+1));

        // the dropdown list
        spinnerList.add(getString(R.string.negative));
        spinnerList.add(getString(R.string.positive));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_edit_budget, null);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, spinnerList);
        final MaterialBetterSpinner betterSpinner = view.findViewById(R.id.budget_spinner);
        betterSpinner.setAdapter(arrayAdapter);

        nameTextView = view.findViewById(R.id.budget_name);
        amountEditText = view.findViewById(R.id.budget_amount);
        spinner = view.findViewById(R.id.budget_spinner);

        nameTextView.setText(key);
        if(value.substring(1,2).equals("-"))
            amountEditText.setText(value.substring(2));
        else
            amountEditText.setText(value.substring(1));

        builder.setView(view)
                .setTitle(getString(R.string.edit_category))
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String symbol = spinner.getText().toString();
                        String negative = getString(R.string.negative);
                        String setSymbol = "";

                        if(symbol.equals(negative))
                            setSymbol = getResources().getString(R.string.negative_symbol);

                        final String budgetText = nameTextView.getText().toString();
                        final String budgetAmount = setSymbol + amountEditText.getText().toString();

                        // changing the name and amount with the one with the same key in the database.
                        DocumentReference docRef = db.collection(Data.loggedInUser).document(selectedYear)
                                .collection("Budget").document("Budget");
                        Map<String, Object> updates = new HashMap<>();
                        updates.put(key, FieldValue.delete());
                        docRef.update(updates);
                        Data.budgetKeyValueStringList.remove(index);

                        Map<String, Object> budgetItem = new HashMap<>();
                        budgetItem.put(budgetText, budgetAmount);
                        Data.editBudgetMap.put(budgetText, budgetAmount);

                        db.collection(Data.loggedInUser).document(selectedYear).collection("Budget").document("Budget").set(budgetItem, SetOptions.merge())
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Data.adapter.notifyDataSetChanged();
                                        Data.budgetKeyValueStringList.add(budgetText + " : " + budgetAmount);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                    }
                                });
                    }
                });

        return builder.create();
    }
}
