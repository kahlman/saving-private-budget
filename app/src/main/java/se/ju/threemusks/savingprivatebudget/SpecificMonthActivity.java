package se.ju.threemusks.savingprivatebudget;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpecificMonthActivity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String budgetKey;
    private Boolean monthLocked = false;
    private Button lockButton;
    private ListView listview;
    int yearIndex;
    int monthIndex;
    int budgets;
    ProgressDialog progress;
    int budgetVSMonth;
    int index;
    private TextView spendingAverage;
    private TextView savedTextView;
    int savedThisMonth;
    int keyValueIndex;
    int income = 0;
    int outcome = 0;
    int monthVSBudgetSum;
    int saved;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // loading
        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.wait_while_loading));
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        setContentView(R.layout.activity_specific_month);
        yearIndex = getIntent().getIntExtra("yearIndex", 1);
        monthIndex = getIntent().getIntExtra("monthIndex",1);
        lockButton = findViewById(R.id.lock_button);
        spendingAverage = findViewById(R.id.spednings_averages);
        savedTextView = findViewById(R.id.savedTextView);


        final String selectedMonth = Data.monthStringResources.get(monthIndex);
        final String selectedYear = Data.years.get(yearIndex);

        TextView monthText = (TextView) findViewById(R.id.month_averages);
        monthText.setText(selectedMonth + " " + selectedYear);

        listview = findViewById(R.id.specificMonthListView);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final int finalposition = position;

                String budgetKeyValueString = Data.budgetKeyValueStringList.get(position);
                budgetKey = budgetKeyValueString.substring(0, (budgetKeyValueString.indexOf(":") - 1));

                Intent intent = new Intent(SpecificMonthActivity.this, MonthPostsActivity.class);
                intent.putExtra("monthPostBudget", Data.budgetSumArray.get(finalposition));
                intent.putExtra("yearIndex", yearIndex);
                intent.putExtra("monthIndex", monthIndex);
                intent.putExtra("clickedIndex", position);
                intent.putExtra("budgetKey", budgetKey);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!progress.isShowing()) {
            progress = new ProgressDialog(this);
            progress.setTitle(getString( R.string.loading));
            progress.setMessage(getString(R.string.wait_while_loading));
            progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
            progress.show();
        }

        final DocumentReference docRef = db.collection(Data.loggedInUser).document(Data.years.get(yearIndex));
        Data.budgetSumArray.clear();
        Data.monthBudgetVSyearBudget.clear();
        Data.lockedMonthsArray.clear();
        Data.monthsLocked.clear();
        Data.savedThisMonth.clear();
        Data.outcomesAndIncomes.clear();
        budgetVSMonth = 0;
        budgets = 0;
        index = 0;
        savedThisMonth = 0;
        keyValueIndex = 0;
        monthVSBudgetSum = 0;
        saved = 0;


        // get locked months
        Data.lockedMonths.clear();
        index = 0;
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Map<String, Object> data = documentSnapshot.getData();
                Map<String, Boolean> map = new HashMap<>();
                Data.lockedMonthsArray.clear();

                if(data != null){
                    for (Map.Entry<String, Object> entry : data.entrySet()) {
                        boolean lockedOrNot;
                        if(entry.getValue().toString().equals("true")){
                            lockedOrNot = true;
                            Data.lockedMonths.put(Integer.toString(index), entry.getKey());
                            index+=1;
                        }else{
                            lockedOrNot = false;
                        }
                        map.put(entry.getKey(), lockedOrNot);
                    }
                }
                Data.lockedMonthsArray.add(map);

            }
        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                progress.dismiss();
            }
        });

        // check if the current month is locked or not, in that case lock the ListView and change the lock button
        checkIfSpecificMonthIsLocked();


        // Calculate spent with the year budget and add to the array. Using that array in adapter.
        getSpecificMonthSumBudget();

        // Calculating income - outcome.
        calculateSavedThisSpecificMonth();

    }

 public void checkIfSpecificMonthIsLocked(){
     db.collection(Data.loggedInUser).document(Data.years.get(yearIndex)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
         @Override
         public void onSuccess(DocumentSnapshot documentSnapshot) {
             Map<String, Object> data = documentSnapshot.getData();

             if(data != null){
                 for (Map.Entry<String, Object> entry : data.entrySet()) {
                     if (entry.getValue().toString().equals("true") && entry.getKey().equals(Data.monthStrings[monthIndex])){
                         listview.setEnabled(false);
                         monthLocked = true;
                         lockButton.setBackgroundResource(R.drawable.locked_icon);
                         for (int i = 0; i < Data.textViewArray.size(); i++) {
                             Data.textViewArray.get(i).setVisibility(View.VISIBLE);

                         }
                     }
                 }
             }
         }
     });
 }

    // When the lockButton is pressed, show all the hidden TextViews and change the button image.
    public void lockButtonPressed(View view){
        listview.setEnabled(false);
        DocumentReference docRef = db.collection(Data.loggedInUser).document(Data.years.get(yearIndex));
        if (monthLocked == false){
            monthLocked = true;

            Data.monthsLocked.put(Data.monthStrings[monthIndex], true);
            docRef.set(Data.monthsLocked, SetOptions.merge());
            lockButton.setBackgroundResource(R.drawable.locked_icon);
            for (int i = 0; i < Data.textViewArray.size(); i++ ) {
                Data.textViewArray.get(i).setVisibility(View.VISIBLE);
            }

        }else{
            listview.setEnabled(true);
            monthLocked = false;
            lockButton.setBackgroundResource(R.drawable.unlocked_icon);
            Data.monthsLocked.remove(Data.monthStrings[monthIndex]);
            Data.monthsLocked.remove(Data.monthStrings[monthIndex]);

            Map<String,Object> updates = new HashMap<>();
            updates.put(Data.monthStrings[monthIndex], FieldValue.delete());
            docRef.update(updates);

            for (int i = 0; i < Data.textViewArray.size(); i++ ) {
                Data.textViewArray.get(i).setVisibility(View.GONE);
            }
        }
    }
    // Get the total sum of all the posts for a category, and also calculating that with the year budget.
    public void getSpecificMonthSumBudget(){
        db.collection(Data.loggedInUser).document(Data.years.get(yearIndex)).collection(Data.monthStrings[monthIndex]).document("MonthSumBudget").get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Map<String, Object> data = documentSnapshot.getData();

                        if(data!=null){
                            for(Map.Entry<String, Object> i : data.entrySet()){

                                // using this function, to calculate for each different categories total sum VS the year budget.
                                calculateMonthVSYearBudget(i.getKey(), Integer.parseInt(i.getValue().toString()));
                            }
                        }else{
                            if(Data.budgetSumArray.size() == 0){
                                for(int i = 0; i < Data.budgetKeyValueStringList.size(); i++){
                                    String budgetNameSubstring = Data.budgetKeyValueStringList.get(i).substring(0,Data.budgetKeyValueStringList.get(i).indexOf(" "));
                                    Data.budgetSumArray.add(budgetNameSubstring + " : " + 0 + " ("+"0" +")");
                                }
                            }
                            Data.budgetAdapter = new MyBudgetSumAdapter(SpecificMonthActivity.this, R.layout.listview_with_budget_text, Data.budgetSumArray);
                            listview.setAdapter(Data.budgetAdapter);
                        }
                    }
                });
    }

    public void calculateMonthVSYearBudget(final String budgetName, final int sum){
        final DocumentReference docRef = db.collection(Data.loggedInUser).document(Data.years.get(yearIndex));
        if(Data.budgetSumArray.size() == 0){
            for(int i = 0; i < Data.budgetKeyValueStringList.size(); i++){
                String budgetNameSubstring = Data.budgetKeyValueStringList.get(i).substring(0,Data.budgetKeyValueStringList.get(i).indexOf(" "));
                Data.budgetSumArray.add(budgetNameSubstring + " : " + 0 + " ("+"0" +")");
            }
        }

        docRef.collection("Budget").document("Budget").get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        int newSum;
                        index = 0;

                        Map<String, Object> data = documentSnapshot.getData();

                        if(data!=null){

                            for (Map.Entry<String, Object> i : data.entrySet()) {
                                newSum = 0;
                                String symbol="";
                                String secondSymbol = "";
                                if(i.getKey().equals(budgetName)){

                                    if(!checkIfNegativeOrPositive(i.getKey()).equals("-")){
                                        newSum = sum - Integer.parseInt(i.getValue().toString());
                                        secondSymbol = getString(R.string.positive_symbol);
                                        if(newSum > 0){
                                            symbol = getString(R.string.positive_symbol);
                                        }

                                    }else{
                                        newSum = Integer.parseInt(i.getValue().toString()) + sum;
                                        secondSymbol = getString(R.string.negative_symbol);
                                    }
                                    if(newSum > 0 && checkIfNegativeOrPositive(i.getKey()).equals("-")){
                                        symbol = getString(R.string.negative_symbol);
                                    }
                                    String firstChar = Integer.toString(newSum).substring(0,1);

                                    if(checkIfNegativeOrPositive(i.getKey()).equals("-") && firstChar.equals("-")){
                                        symbol = getString(R.string.positive_symbol);
                                        newSum *= -1;
                                    }
                                    for(int o = 0; o < Data.budgetSumArray.size(); o++){
                                        if(Data.budgetKeyValueStringList.get(o).substring(0,Data.budgetKeyValueStringList.get(o).indexOf(" ")).equals(i.getKey())){
                                            Data.budgetSumArray.set(o,i.getKey() + " : " + secondSymbol+sum + " ("+symbol+newSum +")");
                                        }
                                    }
                                    if(checkIfNegativeOrPositive(i.getKey()).equals("-") && !firstChar.equals("-")){
                                        newSum *=-1;
                                    }

                                    monthVSBudgetSum += newSum;
                                }
                                index+=1;
                            }
                        }

                    }
                }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                spendingAverage.setText(Integer.toString(monthVSBudgetSum));
                if(monthVSBudgetSum < 0){
                    spendingAverage.setTextColor(Color.RED);
                }else{
                    spendingAverage.setTextColor(Color.GREEN);
                }
                Map<String, Integer> map = new HashMap<>();
                map.put("MonthVSBudget", monthVSBudgetSum);
                docRef.collection(Data.monthStrings[monthIndex]).document("MonthVSBudget").set(map);
                Data.budgetAdapter = new MyBudgetSumAdapter(SpecificMonthActivity.this, R.layout.listview_with_budget_text, Data.budgetSumArray);
                listview.setAdapter(Data.budgetAdapter);
                progress.dismiss();
            }
        });
    }

    // calculating the incomes - outcomes and add the result to the database.
    private void calculateSavedThisSpecificMonth(){
        final DocumentReference docRef = db.collection(Data.loggedInUser).document(Data.years.get(yearIndex));
        outcome = 0;
        income = 0;

        docRef.collection(Data.monthStrings[monthIndex]).document("MonthSumBudget").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Map<String, Object> data = documentSnapshot.getData();
                if(data!=null){
                    for (Map.Entry<String, Object> i : data.entrySet()) {

                        if(checkIfNegativeOrPositive(i.getKey()).equals("-")){
                            outcome+= Integer.parseInt(i.getValue().toString());
                        }else{
                            income += Integer.parseInt(i.getValue().toString());
                        }
                    }
                    saved = income - outcome;
                    savedTextView.setText(Integer.toString(saved));
                    if(saved < 0){
                        savedTextView.setTextColor(Color.RED);
                    }else{
                        savedTextView.setTextColor(Color.GREEN);

                    }
                    Map<String, Integer> map = new HashMap<>();
                    map.put("SavedThisMonth", saved);
                    docRef.collection(Data.monthStrings[monthIndex]).document("SavedThisMonth").set(map);
                }
            }
        });
    }

    // Using this function anytime we need to check if a category is an outcome or income.
    private String checkIfNegativeOrPositive(String string){
        String symbol = "";

        for(int i = 0; i < Data.budgetKeyValueStringList.size(); i++){
            String budgetSubstring = Data.budgetKeyValueStringList.get(i).substring(Data.budgetKeyValueStringList.get(i).indexOf(": ")+2);
            String firstChar = budgetSubstring.substring(0,1);
            String budgetNameSubstring = Data.budgetKeyValueStringList.get(i).substring(0,Data.budgetKeyValueStringList.get(i).indexOf(" "));

            if(budgetNameSubstring.equals(string)){
                if(firstChar.matches("-")){
                    symbol = getResources().getString(R.string.negative_symbol);
                }
            }
        }

        return symbol;
    }

    // Custom adapter, setting the TextView and adding all the hidden TextViews in an array.
    private class MyBudgetSumAdapter extends ArrayAdapter<String>{
        private int layout;
        public MyBudgetSumAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if(convertView == null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);
                final SpecificMonthActivity.ViewHolder viewHolder = new SpecificMonthActivity.ViewHolder();

                String budget = Data.budgetSumArray.get(position).substring(0,Data.budgetSumArray.get(position).indexOf("("));
                String budgetVSYearBudget = Data.budgetSumArray.get(position).substring(Data.budgetSumArray.get(position).indexOf("("));

                viewHolder.budgetTextOne = convertView.findViewById(R.id.list_item_textOne);
                viewHolder.budgetTextOne.setText(budget);
                viewHolder.budgetTextTwo = convertView.findViewById(R.id.list_item_textTwo);
                viewHolder.budgetTextTwo.setText(budgetVSYearBudget);
                viewHolder.lockButton = convertView.findViewById(R.id.lock_button);
                Data.textViewArray.add(viewHolder.budgetTextTwo);
                if(monthLocked){
                    viewHolder.budgetTextTwo.setVisibility(View.VISIBLE);
                }

                convertView.setTag(viewHolder);

            }
            return  convertView;
        }
    }

    public class ViewHolder{
        TextView budgetTextOne;
        TextView budgetTextTwo;
        Button lockButton;
    }
}

