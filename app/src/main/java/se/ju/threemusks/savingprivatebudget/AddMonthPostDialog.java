package se.ju.threemusks.savingprivatebudget;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

public class AddMonthPostDialog extends AppCompatDialogFragment {
    private EditText inputEditText;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String selectedYear;
    private String budgetName;
    private String selectedMonth;
    private int numberOfBudgets = 0;
    private Map<String, Object> budget;
    private int budgetSum;
    private int clickedIndex;
    ProgressDialog progress;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        budgetSum=0;


        final LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_with_edit_text, null);

        selectedYear = getArguments().getString("selected_year");
        budgetName = getArguments().getString("budget_name");
        selectedMonth = getArguments().getString("selected_month");
        inputEditText = view.findViewById(R.id.dialog_edit_text);
        clickedIndex = getArguments().getInt("clickedIndex");

        builder.setView(view)
                .setTitle(getString(R.string.add_new_post))
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    final String input = inputEditText.getText().toString();
                        progress = new ProgressDialog(getActivity());
                        progress.setTitle(getString(R.string.loading));
                        progress.setMessage(getString(R.string.wait_while_loading));
                        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
                        progress.show();

                    Data.monthPostsString.add(input);

                        // Loop through the database and get the number of posts.

                     db.collection(Data.loggedInUser).document(selectedYear)
                                .collection(selectedMonth).document(budgetName).get()
                                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        Map<String, Object> data = documentSnapshot.getData();

                                        budgetSum += Integer.parseInt(input);

                                        // Add to the database where the key is the number of posts in the database.
                                        if(data != null)  {
                                            for (Map.Entry<String, Object> i : data.entrySet()) {
                                                numberOfBudgets+=1;
                                                budget = new HashMap<>();
                                                budget.put(Integer.toString(numberOfBudgets), input);
                                                budgetSum += Integer.parseInt(i.getValue().toString());
                                            }


                                            db.collection(Data.loggedInUser).document(selectedYear).collection(selectedMonth).document(budgetName).set(budget, SetOptions.merge())
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            numberOfBudgets=0;

                                                            // add the sum to the database.
                                                            // updating the total sum of that category.

                                                            Map<String, Object> budgetItem = new HashMap<>();
                                                            budgetItem.put(budgetName, budgetSum);

                                                            Data.budgetSum = budgetSum;
                                                            if(!Data.budgetSumArray.get(clickedIndex).isEmpty())
                                                            {
                                                                Data.budgetSumArray.set(clickedIndex, budgetName + ": " + Integer.toString(budgetSum));
                                                            }
                                                            else{
                                                                Data.budgetSumArray.add(budgetName + ": 0");
                                                            }

                                                            db.collection(Data.loggedInUser).document(selectedYear).collection(selectedMonth).document("MonthSumBudget").set(budgetItem, SetOptions.merge())
                                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                        @Override
                                                                        public void onSuccess(Void aVoid) {
                                                                            progress.dismiss();
                                                                            Data.adapter.notifyDataSetChanged();
                                                                        }
                                                                    });

                                                        }
                                                    });
                                        }else{
                                            budget = new HashMap<>();

                                            budget.put("0", input);
                                            db.collection(Data.loggedInUser).document(selectedYear).collection(selectedMonth).document(budgetName).set(budget, SetOptions.merge())
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            progress.dismiss();
                                                        }
                                                    });
                                            // If the database if empty we add the post to the database here.

                                            Map<String, Object> budgetItem = new HashMap<>();
                                            budgetItem.put(budgetName, budgetSum);

                                            // adding the new sum of that category to the database.

                                            db.collection(Data.loggedInUser).document(selectedYear).collection(selectedMonth).document("MonthSumBudget").set(budgetItem, SetOptions.merge())
                                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            Data.adapter.notifyDataSetChanged();
                                                        }
                                                    });
                                        }
                                    }
                                });

                                    }
                                });

        return builder.create();
    }
}