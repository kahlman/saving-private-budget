package se.ju.threemusks.savingprivatebudget;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MonthSelectionActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private int clickedInt;
    private TextView headerTextView;
    private TextView monthVSBudgetTextView;
    private String clickedYear;
    private int index;
    private int avgMonthVSBudget;
    ProgressDialog progress;
    ArrayList<Integer> avgMonthVSBudgetArray = new ArrayList<>();
    private int indexI;
    int total;
    int totalSavedThisYear;
    private TextView totalSavedThisYearTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_selection);
        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.wait_while_loading));
        progress.setCancelable(false);
        progress.show();
        Data.monthStringResources.clear();

        // Adding to the array the months string name depending on current language.
        // doing this because we cannot use the getString in the Data class.
        Data.monthStringResources.add(getString(R.string.January));
        Data.monthStringResources.add(getString(R.string.February));
        Data.monthStringResources.add(getString(R.string.March));
        Data.monthStringResources.add(getString(R.string.April));
        Data.monthStringResources.add(getString(R.string.May));
        Data.monthStringResources.add(getString(R.string.June));
        Data.monthStringResources.add(getString(R.string.July));
        Data.monthStringResources.add(getString(R.string.August));
        Data.monthStringResources.add(getString(R.string.September));
        Data.monthStringResources.add(getString(R.string.October));
        Data.monthStringResources.add(getString(R.string.November));
        Data.monthStringResources.add(getString(R.string.December));

        clickedInt = getIntent().getIntExtra("yearIndex", 1);
        clickedYear = Data.years.get(clickedInt);
        ListView listView = findViewById(R.id.monthListView);
        listView.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                Data.monthStringResources
        ));

        headerTextView = findViewById(R.id.month_averages);
        headerTextView.setText(clickedYear);
        monthVSBudgetTextView = findViewById(R.id.avg_monthVSBudget);
        totalSavedThisYearTextView = findViewById(R.id.savedTextView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                Intent intent = new Intent(MonthSelectionActivity.this, SpecificMonthActivity.class);
                intent.putExtra("monthIndex", position);
                intent.putExtra("yearIndex", clickedInt);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        final DocumentReference docRef = db.collection(Data.loggedInUser).document(clickedYear);
        index = 0;
        indexI = 0;
        avgMonthVSBudget = 0;
        avgMonthVSBudgetArray.clear();
        totalSavedThisYear = 0;

        if(!progress.isShowing()) {
            progress = new ProgressDialog(this);
            progress.setTitle(getString(R.string.loading));
            progress.setMessage(getString(R.string.wait_while_loading));
            progress.setCancelable(false);
            progress.show();
        }

        // Get all the current locked months, and adding it to the local array.

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Map<String, Object> data = documentSnapshot.getData();
                Map<String, Boolean> map = new HashMap<>();
                Data.lockedMonthsArray.clear();

                if(data != null){

                    for (Map.Entry<String, Object> entry : data.entrySet()) {
                        boolean lockedOrNot;
                        if(entry.getValue().toString().equals("true")){
                            lockedOrNot = true;
                            Data.lockedMonths.put(Integer.toString(index), entry.getKey());
                            index+=1;
                        }else{
                            lockedOrNot = false;
                        }
                        map.put(entry.getKey(), lockedOrNot);
                    }
                    if(Data.lockedMonths.size() == 0)
                    {
                        monthVSBudgetTextView.setText("0");
                        progress.dismiss();
                    }
                }else{
                    progress.dismiss();
                }
                Data.lockedMonthsArray.add(map);

            }
        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                for (Map.Entry<String, String> entry : Data.lockedMonths.entrySet()) {

                    // On complete, get all the MonthVSBudget from all the locked months, and calculate the average, and then set the TextView to the result.

                    docRef.collection(entry.getValue()).document("MonthVSBudget").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            Map<String, Object> data = documentSnapshot.getData();
                            if(data != null) {
                                for (Map.Entry<String, Object> entry : data.entrySet()) {
                                    avgMonthVSBudgetArray.add(Integer.parseInt(entry.getValue().toString()));
                                }
                            }else{
                                progress.dismiss();
                            }
                            indexI+=1;
                        }
                    }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(indexI == Data.lockedMonths.size()){
                                total =0;
                                for(int i : avgMonthVSBudgetArray){
                                    total += i;
                                }
                                total = total / Data.lockedMonths.size();
                                monthVSBudgetTextView.setText(""+total);
                                if(total < 0){
                                    monthVSBudgetTextView.setTextColor(Color.RED);
                                }else if(total > 0){
                                    monthVSBudgetTextView.setTextColor(Color.GREEN);
                                }
                                progress.dismiss();
                            }
                            if(Data.continueORNot){
                                Data.continueORNot = false;
                                Intent intent = new Intent(MonthSelectionActivity.this, SpecificMonthActivity.class);
                                intent.putExtra("monthIndex", Data.lockedMonths.size());
                                intent.putExtra("yearIndex", Data.years.size()-1);
                                startActivity(intent);
                            }
                        }
                    });

                    // Also get the SavedThisMonth for all the locked month, and do calculations.

                    docRef.collection(entry.getValue()).document("SavedThisMonth").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            Map<String, Object> data = documentSnapshot.getData();
                            if(data != null) {
                                for (Map.Entry<String, Object> entry : data.entrySet()) {
                                    totalSavedThisYear += Integer.parseInt(entry.getValue().toString());
                                }
                                totalSavedThisYearTextView.setText(Integer.toString(totalSavedThisYear));
                                if(totalSavedThisYear > 0){
                                    totalSavedThisYearTextView.setTextColor(Color.GREEN);
                                }else if(totalSavedThisYear < 0){
                                    totalSavedThisYearTextView.setTextColor(Color.RED);
                                }
                                Map<String, Integer> totalSavedMap = new HashMap<>();
                                totalSavedMap.put("TotalSavedThisYear", totalSavedThisYear);
                                docRef.set(totalSavedMap, SetOptions.merge());
                            }
                            Data.savedThisYear = totalSavedThisYear;

                        }
                    });
                }
            }
        });

    }

    // Get the set budgets to that year.

    @Override
    protected void onStart() {
        super.onStart();
        db.collection(Data.loggedInUser).document(clickedYear).collection("Budget").document("Budget").get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        Map<String, Object> data = documentSnapshot.getData();
                        Data.budgetKeyValueStringList.clear();

                        if(data != null)
                            for (Map.Entry<String, Object> i : data.entrySet()) {
                                Data.budgetKeyValueStringList.add(i.getKey() + " : " + i.getValue());
                            }
                    }
                });
    }

    public void editBudgetClick(View view)
    {
        Intent intent = new Intent(this, BudgetActivity.class);
        intent.putExtra("yearIndex", clickedInt);
        startActivity(intent);
    }

    public void monthAveragesButtonPressed(View view){
        Intent intent = new Intent(this, MonthAveragesActivity.class);
        intent.putExtra("yearIndex", clickedInt);
        startActivity(intent);
    }
}
