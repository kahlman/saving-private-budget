package se.ju.threemusks.savingprivatebudget;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MonthAveragesActivity extends AppCompatActivity {
    private int yearIndex;
    private  TextView monthAveragesTextView;
    private TextView savedAverages;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ArrayList<String> budgetAverages;
    private int index;
    private ProgressDialog progress;
    DocumentReference docRef;
    private int monthSum;
    Map<String, Integer> averageMap = new HashMap<>();
    private ArrayList<String> averageBudgetArray = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private ListView listView;
    private int budgetIndex;
    private int lockedMonthsIndex;
    int sum;
    int newsum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_averages);

        budgetAverages = new ArrayList<>();
        listView = findViewById(R.id.month_averages_listview);

        // loading
        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.wait_while_loading));
        progress.setCancelable(false);
        progress.show();
        averageMap.clear();
        averageBudgetArray.clear();
        budgetIndex = 0;
        sum = 0;

        yearIndex = getIntent().getIntExtra("yearIndex", -1);
        docRef = db.collection(Data.loggedInUser).document(Data.years.get(yearIndex));
        monthAveragesTextView = findViewById(R.id.month_averages);
        monthAveragesTextView.setText(getString(R.string.month_averages) + " " + Data.years.get(yearIndex));
        savedAverages = findViewById(R.id.spednings_averages);

        // get the locked months
        Data.lockedMonths.clear();
        index = 0;
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Map<String, Object> data = documentSnapshot.getData();
                Map<String, Boolean> map = new HashMap<>();
                Data.lockedMonthsArray.clear();

                if(data != null){
                    for (Map.Entry<String, Object> entry : data.entrySet()) {
                        boolean lockedOrNot;
                        if(entry.getValue().toString().equals("true")){
                            lockedOrNot = true;
                            Data.lockedMonths.put(Integer.toString(index), entry.getKey());
                            index+=1;
                        }else{
                            lockedOrNot = false;
                        }
                        map.put(entry.getKey(), lockedOrNot);

                    }
                }
                Data.lockedMonthsArray.add(map);

            }
        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                calculateMonthAverages();
            }
        });

    }

    // get the sum of all different categories in this year, and divide it with the number of locked months to get an average

    public void calculateMonthAverages(){
        lockedMonthsIndex = 0;
        if(Data.lockedMonths.size() == 0){
            progress.dismiss();
            Toast toast = Toast.makeText(getBaseContext(), R.string.no_locked_months, Toast.LENGTH_LONG);
            toast.show();
        }
        for(int i = 0; i < Data.lockedMonths.size(); i++){

            final String month = Data.lockedMonths.get(Integer.toString(i));
            docRef.collection(month).document("MonthSumBudget").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    Map<String, Object> data = documentSnapshot.getData();
                    lockedMonthsIndex+=1;

                    if(data !=null){

                        for (Map.Entry<String, Object> entry : data.entrySet()) {
                            monthSum = 0;
                            String bName = entry.getKey();
                            if(averageMap.containsKey(bName)){
                                monthSum += averageMap.get(entry.getKey()) + Integer.parseInt(entry.getValue().toString());
                                averageMap.remove(bName);
                                averageMap.put(entry.getKey(), monthSum);

                            }else{
                                averageMap.put(bName, Integer.parseInt(entry.getValue().toString()));
                            }

                        }
                    }

                    // add calculated average to the averageBudgetArray

                if(lockedMonthsIndex == Data.lockedMonths.size()) {
                    if (averageMap.size() != 0) {
                        for (Map.Entry<String, Integer> entry : averageMap.entrySet()) {

                            int bAverage = entry.getValue() / Data.lockedMonths.size();
                            String symbol = "";

                            // run the function and pass the averageMap and average as parameters
                            // then calculate the difference with the set year budget
                            int averageVSBudget = calculateAverageVSYearBudget(averageMap, bAverage);
                            if (averageVSBudget > 0) {
                                symbol = getString(R.string.positive_symbol);
                            }
                            if(averageVSBudget > 0 && checkIfNegativeOrPositive(entry.getKey()).equals("-")){
                                symbol = getString(R.string.negative_symbol);
                            }
                            if(averageVSBudget < 0 && checkIfNegativeOrPositive(entry.getKey()).equals("-")){
                                symbol = getString(R.string.positive_symbol);
                                String newSum = Integer.toString(averageVSBudget).substring(1);
                                averageVSBudget = Integer.parseInt(newSum);
                            }
                     averageBudgetArray.add(entry.getKey() + " " + bAverage +" "+ "(" + symbol + averageVSBudget + ")");
                        }
                    }
                }
                
                }
            }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    sum = (Data.savedThisYear / Data.lockedMonths.size());
                    savedAverages.setText(Integer.toString(sum));
                    if(sum > 0){
                        savedAverages.setTextColor(Color.GREEN);
                    }else if (sum < 0){
                        savedAverages.setTextColor(Color.RED);
                    }
                    adapter = new ArrayAdapter<>(MonthAveragesActivity.this, android.R.layout.simple_list_item_1, averageBudgetArray);
                    listView.setAdapter(adapter);
                    listView.setEnabled(false);
                    progress.dismiss();
                }
            });
        }
    }

    // The function that calculates the difference between the calculated average for a category, with the set year budget for that category.
    public int calculateAverageVSYearBudget(Map<String, Integer> map ,int sum){
        int budgetSubstring=0;
        if(budgetIndex < map.size()) {
            String firstChar = Data.budgetKeyValueStringList.get(budgetIndex).substring(Data.budgetKeyValueStringList.get(budgetIndex).indexOf(": ") + 2);

            budgetSubstring = Integer.parseInt(Data.budgetKeyValueStringList.get(budgetIndex).substring(Data.budgetKeyValueStringList.get(budgetIndex).indexOf(": ") + 2));

            if (firstChar.startsWith("-")) {
                budgetSubstring += sum;
            } else {
                budgetSubstring = sum - budgetSubstring;
            }
            budgetIndex += 1;
        }

        return budgetSubstring;
    }

    // a function that checks if a category is an income or and outcome, the function returns a "-" string if it's an outcome.
    private String checkIfNegativeOrPositive(String string){
        String symbol = "";

        for(int i = 0; i < Data.budgetKeyValueStringList.size(); i++){
            String budgetSubstring = Data.budgetKeyValueStringList.get(i).substring(Data.budgetKeyValueStringList.get(i).indexOf(": ")+2);
            String firstChar = budgetSubstring.substring(0,1);
            String budgetNameSubstring = Data.budgetKeyValueStringList.get(i).substring(0,Data.budgetKeyValueStringList.get(i).indexOf(" "));

            if(budgetNameSubstring.equals(string)){
                if(firstChar.matches("-")){
                    symbol = getResources().getString(R.string.negative_symbol);
                }
            }
        }
        return symbol;
    }
}
