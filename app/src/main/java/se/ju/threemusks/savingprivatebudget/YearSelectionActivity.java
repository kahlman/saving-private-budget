package se.ju.threemusks.savingprivatebudget;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YearSelectionActivity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    ListView listView;
    ProgressDialog progress;
    int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year_selection);
        progress = new ProgressDialog(this);
        progress.setTitle(getString(R.string.loading));
        progress.setMessage(getString(R.string.wait_while_loading));
        progress.setCancelable(false);
        progress.show();
        Data.years.clear();

        listView = findViewById(R.id.yearListView);

            // Get all years in the database.
            db.collection(Data.loggedInUser).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        QuerySnapshot data = task.getResult();

                        if (data.isEmpty()) {
                            Data.adapter = new MyYearsAdapter(YearSelectionActivity.this, R.layout.listview_with_delete, Data.years);
                            listView.setAdapter(Data.adapter);
                            progress.dismiss();
                        }

                        // When loading data is complete, create the adapter.
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Data.years.add(document.getId());

                            for (int i = 0; i < Data.monthStrings.length; i++) {

                                db.collection(Data.loggedInUser).document(document.getId()).collection(Data.monthStrings[i]).document("MonthSumBudget").get()
                                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                Map<String, Object> data = documentSnapshot.getData();
                                                if (data != null) {
                                                    for (Map.Entry<String, Object> i : data.entrySet()) {
                                                        if(checkIfNegativeOrPositive(i.getKey()).equals("-")){
                                                        }
                                                    }
                                                    db.collection(Data.loggedInUser).document(Data.years.get(Data.years.size()-1)).set(Data.totalYearsSumMap, SetOptions.merge());
                                                }

                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                        // send the user to the month after the last locked month, where he should continue adding posts.
                                        // need to send the user through all of the activities for loading data purposes.

                                        if(Data.years.size() != 0 && Data.adapter == null){
                                            // check for number of locked months
                                            Data.lockedMonths.clear();
                                            index = 0;
                                            DocumentReference docRef = db.collection(Data.loggedInUser).document(Data.years.get(Data.years.size()-1));

                                            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {
                                                    Map<String, Object> data = documentSnapshot.getData();
                                                    Map<String, Boolean> map = new HashMap<>();
                                                    Data.lockedMonthsArray.clear();

                                                    if(data != null){
                                                        for (Map.Entry<String, Object> entry : data.entrySet()) {
                                                            boolean lockedOrNot;
                                                            if(entry.getValue().toString().equals("true")){
                                                                lockedOrNot = true;
                                                                Data.lockedMonths.put(Integer.toString(index), entry.getKey());
                                                                index+=1;
                                                            }else{
                                                                lockedOrNot = false;
                                                            }
                                                            map.put(entry.getKey(), lockedOrNot);

                                                        }
                                                    }
                                                    Data.lockedMonthsArray.add(map);
                                                }
                                            }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if(Data.lockedMonths.size()!= 0){
                                                        Data.continueORNot = true;
                                                        Intent intent = new Intent(YearSelectionActivity.this, MonthSelectionActivity.class);
                                                        intent.putExtra("yearIndex", Data.years.size()-1);
                                                        startActivity(intent);
                                                    }
                                                }
                                            });

                                        }
                                        if(Data.adapter == null) {
                                            Data.adapter = new MyYearsAdapter(YearSelectionActivity.this, R.layout.listview_with_delete, Data.years);
                                            listView.setAdapter(Data.adapter);
                                            progress.dismiss();
                                        }else{
                                            Data.adapter = new MyYearsAdapter(YearSelectionActivity.this, R.layout.listview_with_delete, Data.years);
                                            listView.setAdapter(Data.adapter);
                                            progress.dismiss();
                                        }
                                    }
                                });

                            }
                        }

                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast toast = Toast.makeText(getBaseContext(), "Something went wrong", Toast.LENGTH_SHORT);
                    toast.show();
                }
            });

        //  Wait for click on a year
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(YearSelectionActivity.this, MonthSelectionActivity.class);
                intent.putExtra("yearIndex", position);
                intent.putExtra("continue", false);
                startActivity(intent);

            }
        });

    }

    public void addYearButtonClicked(View view)
    {
        openAddYearDialog();
    }

    public void infoButtonClicked(View view){
        Intent intent = new Intent(this, InformationActivity.class);
        startActivity(intent);
    }

    public void openAddYearDialog(){
        AddYearDialog addYearDialog = new AddYearDialog();
        addYearDialog.show(getSupportFragmentManager(),  Data.loggedInUser);
    }

        // Custom adapter, so we can add on click listeners to all of the delete buttons in the listview

    private class MyYearsAdapter extends ArrayAdapter<String>{
        private int layout;
        public MyYearsAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder mainViewHolder;
            if(convertView == null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);
                ViewHolder viewHolder = new ViewHolder();

                viewHolder.year = convertView.findViewById(R.id.list_item);
                viewHolder.year.setText(Data.years.get(position));

                viewHolder.bn_delete = convertView.findViewById(R.id.list_del_button);
                viewHolder.bn_delete.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {

                        new AlertDialog.Builder(YearSelectionActivity.this)
                                .setTitle(getString(R.string.delete_year))
                                .setMessage(getString(R.string.confirm_delete_year))
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        db.collection(Data.loggedInUser).document(getItem(position)).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                            @Override
                                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override

                                            // Delete the year and all subcollections.
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                for(String month : Data.monthStrings){
                                                    db.collection(Data.loggedInUser).document(getItem(position)).collection(month).document("MonthSumBudget").delete();
                                                    db.collection(Data.loggedInUser).document(getItem(position)).collection(month).document("MonthVSBudget").delete();
                                                    db.collection(Data.loggedInUser).document(getItem(position)).collection(month).document("SavedThisMonth").delete();
                                                    for(String budget : Data.budgetKeyValueStringList){
                                                        budget = budget.substring(0, budget.indexOf(" "));
                                                        db.collection(Data.loggedInUser).document(getItem(position)).collection(month).document(budget).delete();
                                                    }
                                                    db.collection(Data.loggedInUser).document(getItem(position)).collection(month).document("MonthSumBudget").delete();

                                                }
                                                db.collection(Data.loggedInUser).document(getItem(position)).collection("Budget").document("Budget").delete();
                                                db.collection(Data.loggedInUser).document(getItem(position)).delete();
                                                Data.years.remove(position);
                                                notifyDataSetChanged();
                                            }
                                        });

                                    }
                                })
                                .setNegativeButton(getString(R.string.no), null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });
                convertView.setTag(viewHolder);

            }else{
                mainViewHolder = (ViewHolder) convertView.getTag();
                mainViewHolder.year.setText(getItem(position));
            }
            return  convertView;
        }
    }

    public class ViewHolder{
        TextView year;
        Button bn_delete;
    }

    // using this to check if a category is an outcome or income.
    private String checkIfNegativeOrPositive(String string){
        String symbol = "";

        for(int i = 0; i < Data.budgetKeyValueStringList.size(); i++){
            String budgetSubstring = Data.budgetKeyValueStringList.get(i).substring(Data.budgetKeyValueStringList.get(i).indexOf(": ")+2);
            String firstChar = budgetSubstring.substring(0,1);
            String budgetNameSubstring = Data.budgetKeyValueStringList.get(i).substring(0,Data.budgetKeyValueStringList.get(i).indexOf(" "));

            if(budgetNameSubstring.equals(string)){
                if(firstChar.matches("-")){
                    symbol = getResources().getString(R.string.negative_symbol);
                }
            }
        }

        return symbol;
    }
}

