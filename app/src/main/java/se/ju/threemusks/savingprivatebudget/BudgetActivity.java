package se.ju.threemusks.savingprivatebudget;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BudgetActivity extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String budgetYear;
    private TextView headerTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);
        ListView listView = findViewById(R.id.budgetListView);

        int yearIndex = getIntent().getIntExtra("yearIndex", 1);
        budgetYear = Data.years.get(yearIndex);

        headerTextView = findViewById(R.id.budgetheader_textview);
        String string = getString(R.string.edit_budget);
        headerTextView.setText(string + " " + budgetYear);

        Data.adapter = new BudgetAdapter(this, R.layout.listview_with_edit_and_delete, Data.budgetKeyValueStringList);
        listView.setAdapter(Data.adapter);

        Data.adapter.notifyDataSetChanged();
    }

    public void openAddBudgetPostDialog(View view) {
        AddBudgetPostDialog addBudgetPostDialog = new AddBudgetPostDialog();
        addBudgetPostDialog.show(getSupportFragmentManager(), budgetYear);
    }

    public void openEditBudgetPostDialog(int index){
        EditBudgetPostDialog editBudgetPostDialog = new EditBudgetPostDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        editBudgetPostDialog.setArguments(bundle);
        editBudgetPostDialog.show(getSupportFragmentManager(), budgetYear);
    }

    public void deleteBudget(final int index) {

        new AlertDialog.Builder(BudgetActivity.this)
                .setTitle(getString(R.string.delete_category))
                .setMessage(getString(R.string.confirm_delete_category))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        DocumentReference docRef = db.collection(Data.loggedInUser).document(budgetYear)
                                .collection("Budget").document("Budget");

                        String budgetKeyValueString = Data.budgetKeyValueStringList.get(index);
                        String budgetKey = budgetKeyValueString.substring(0, (budgetKeyValueString.indexOf(":") - 1));

                        Map<String, Object> updates = new HashMap<>();
                        updates.put(budgetKey, FieldValue.delete());
                        docRef.update(updates);

                        for (String budgetMonth : Data.monthStrings) {
                            db.collection(Data.loggedInUser).document(budgetYear).collection(budgetMonth).document(budgetKey).delete();
                            db.collection(Data.loggedInUser).document(budgetYear).collection(budgetMonth).document("MonthSumBudget").update(updates);
                        }

                        Data.budgetKeyValueStringList.remove(index);
                        Data.adapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private class BudgetAdapter extends ArrayAdapter<String> {
        private int layout;
        public BudgetAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
            layout = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            BudgetActivity.BudgetViewHolder mainViewHolder;
            if (convertView == null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);
                BudgetViewHolder viewHolder = new BudgetViewHolder();

                viewHolder.budget = convertView.findViewById(R.id.list_item_textview);
                viewHolder.budget.setText(Data.budgetKeyValueStringList.get(position));

                viewHolder.bn_edit = convertView.findViewById(R.id.list_edit_btn);
                viewHolder.bn_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openEditBudgetPostDialog(position);
                    }
                });

                viewHolder.bn_delete = convertView.findViewById(R.id.list_del_btn);
                viewHolder.bn_delete.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        deleteBudget(position);
                    }
                });

                convertView.setTag(viewHolder);

            } else {
                mainViewHolder = (BudgetActivity.BudgetViewHolder) convertView.getTag();
                mainViewHolder.budget.setText(getItem(position));
            }
            return  convertView;
        }
    }

    public class BudgetViewHolder{
        TextView budget;
        Button bn_edit;
        Button bn_delete;
    }
}
