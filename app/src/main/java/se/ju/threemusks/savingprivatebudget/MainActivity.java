package se.ju.threemusks.savingprivatebudget;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


// The main screen, only contains two buttons that will send the user to the next activity.
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void infoButtonClicked(View view){
        Intent intent = new Intent(this, InformationActivity.class);
        startActivity(intent);
    }

    public void enlistButtonClicked(View view){
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }
}
