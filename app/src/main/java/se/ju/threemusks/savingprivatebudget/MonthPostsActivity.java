package se.ju.threemusks.savingprivatebudget;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

public class MonthPostsActivity extends AppCompatActivity {

    private TextView headerTextView;
    private ListView listView;
    private ArrayAdapter adapter;
    private int yearIndex;
    private int monthIndex;
    private String budgetName;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    int clickedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_month_posts);

        headerTextView = findViewById(R.id.month_posts_header_textview);
        listView = findViewById(R.id.month_post_ListView);

        yearIndex = getIntent().getIntExtra("yearIndex", 1);
        final String yearText = Data.years.get(yearIndex);
        monthIndex = getIntent().getIntExtra("monthIndex",1);
        final String monthText = Data.monthStringResources.get(monthIndex);
        String budgetKeyValueString = getIntent().getExtras().getString("monthPostBudget");
        budgetName = budgetKeyValueString.substring(0, (budgetKeyValueString.indexOf(":")-1));
        clickedIndex = getIntent().getIntExtra("clickedIndex",-1);
        headerTextView.setText(budgetName + " " + monthText + " " + yearText);

        // Get all the posts from the database.

        db.collection(Data.loggedInUser).document(yearText).collection(Data.monthStrings[monthIndex]).document(budgetName).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {

                        Map<String, Object> data = documentSnapshot.getData();
                        Data.monthPostsString.clear();

                        if(data != null)  {
                            for (Map.Entry<String, Object> i : data.entrySet()) {
                                Data.monthPostsString.add(i.getValue().toString());
                            }
                        }
                        Data.adapter = new ArrayAdapter(MonthPostsActivity.this, android.R.layout.simple_list_item_1, Data.monthPostsString);
                        listView.setAdapter(Data.adapter);
                    }
                });

        // You will be able to delete posts by clicking on them in the listview.

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                new AlertDialog.Builder(MonthPostsActivity.this)
                        .setTitle(getString(R.string.delete_post))
                        .setMessage(getString(R.string.confirm_delete_post))

                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final DocumentReference docRef = db.collection(Data.loggedInUser).document(yearText)
                                        .collection(monthText).document(budgetName);

                              Map<String, Object> updates = new HashMap<>();
                                updates.put(Integer.toString(position), FieldValue.delete());
                                docRef.update(updates);

                                //lower the keyvalue with 1 after the one we deleted, because we add posts with their key as the number of how many posts
                                // that is currently in the database.

                                docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        Map<String, Object> data = documentSnapshot.getData();
                                        Map<String, Object> map = new HashMap<>();
                                        int newKey;

                                        if(data!=null){
                                            for (Map.Entry<String, Object> i : data.entrySet()) {
                                                newKey = 0;
                                                if(Integer.parseInt(i.getKey()) > position){
                                                    newKey = Integer.parseInt(i.getKey()) - 1;
                                                    map.put(Integer.toString(newKey), i.getValue());
                                                    docRef.set(map);
                                                }else{
                                                    map.put(i.getKey(), i.getValue());
                                                }
                                            }
                                        }
                                    }
                                });

                                // when u have deleted a post, the sum of the posts in the database needs to be updated.

                                db.collection(Data.loggedInUser).document(yearText).collection(monthText).document("MonthSumBudget").get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        Map<String, Object> data = documentSnapshot.getData();
                                        if(data != null) {
                                            for (Map.Entry<String, Object> entry : data.entrySet()) {
                                                if(entry.getKey().equals(budgetName)){
                                                    Map<String, Object> map = new HashMap<>();
                                                    int sum = Integer.parseInt(entry.getValue().toString());
                                                    int newsum = sum - Integer.parseInt(Data.monthPostsString.get(position));
                                                    Data.monthPostsString.remove(position);
                                                    Data.adapter.notifyDataSetChanged();
                                                    map.put(entry.getKey(), newsum);
                                                    db.collection(Data.loggedInUser).document(yearText).collection(monthText).document("MonthSumBudget").set(map, SetOptions.merge());
                                                }
                                            }
                                        }
                                        // delete the document if the size is 0, do avoid bug with the database when you want to add more posts.
                                        if(Data.monthPostsString.size() == 0){
                                            docRef.delete();
                                        }
                                    }
                                });

                            }
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });
    }

    // open the AddMonthPostDialog when you want to add post
    public void addNewPost(View view){
            AddMonthPostDialog mDialog = new AddMonthPostDialog();
            Bundle bundle = new Bundle();
            bundle.putString("selected_year", Data.years.get(yearIndex));
            bundle.putString("budget_name", budgetName);
            bundle.putString("selected_month", Data.monthStrings[monthIndex]);
            bundle.putInt("clickedIndex",clickedIndex);
            mDialog.setArguments(bundle);
            mDialog.show(getSupportFragmentManager(), "budget");
    }


}
