package se.ju.threemusks.savingprivatebudget;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

// storing some locally data for further use.
public class Data extends MainActivity {

   public static String loggedInUser;
   public static ArrayList<String> years = new ArrayList<String>();
   public static ArrayList<String> budgetKeyValueStringList = new ArrayList<>();
   public static ArrayList<String> monthPostsString = new ArrayList<>();
   public static String [] monthStrings = { "January", "February", "March","April", "May", "June", "July", "August", "September", "October", "November", "December" };
   public static ArrayList<String> monthStringResources = new ArrayList<>();
   public static ArrayAdapter<String> adapter;
   public static int budgetSum = 0;
   public static ArrayList<String> budgetSumArray = new ArrayList<>();
   public static ArrayAdapter<String> budgetAdapter;
   public static ArrayList<TextView> textViewArray = new ArrayList<>();
   public static ArrayList<Integer> monthBudgetVSyearBudget = new ArrayList<>();
   public static Map<String, String> totalYearsSumMap = new HashMap<>();
   public static Map<String, Boolean> monthsLocked = new HashMap<>();
   public static ArrayList<Map<String, Boolean>> lockedMonthsArray = new ArrayList<>();
   public static Map<String, String> lockedMonths = new HashMap<>();
   public static Map<String, Object> editBudgetMap = new HashMap<>();
   public static boolean continueORNot = false;
   public static Map<String, String> outcomesAndIncomes = new HashMap<>();
   public static Map<String, String> savedThisMonth = new HashMap<>();
   public static int savedThisYear;

}

