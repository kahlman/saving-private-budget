package se.ju.threemusks.savingprivatebudget;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddYearDialog extends AppCompatDialogFragment {
    private EditText inputEditText;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_with_edit_text, null);
        inputEditText = view.findViewById(R.id.dialog_edit_text);

        builder.setView(view)
                .setTitle(getString(R.string.add_new_year))
                .setNegativeButton((R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton((R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String yearText = inputEditText.getText().toString();

                        Data.years.add(yearText);
                        Data.adapter.notifyDataSetChanged();

                        Map<String, Object> year = new HashMap<>();
                        year.put("Year", yearText);

                        db.collection(Data.loggedInUser).document(yearText).set(year);

                    }
                });
        return builder.create();
    }
}
