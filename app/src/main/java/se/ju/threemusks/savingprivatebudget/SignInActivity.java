package se.ju.threemusks.savingprivatebudget;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;


public class SignInActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {

    private LinearLayout Login_Layout;
    private SignInButton SignIn;
    private Button SignOut;
    private Button Continue;
    private TextView Name,Email;
    private GoogleApiClient googleApiClient;

    private static final int REQ_CODE = 9001;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true)
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        db.setFirestoreSettings(settings);
        SignIn =  findViewById(R.id.bn_login);
        Login_Layout = findViewById(R.id.login_layout);
        SignOut = findViewById(R.id.bn_logout);
        Name = findViewById(R.id.login_name);
        Email = findViewById(R.id.login_address);
        Continue = findViewById(R.id.bn_continue);
        SignIn.setOnClickListener(this);
        SignOut.setOnClickListener(this);
        Login_Layout.setVisibility(View.GONE);
        Data.years.clear();

        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();

    }

    // this runs every time something is clicked, using switch do run different functions depending which button that was clicked.
    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.bn_login:
                signIn();
                break;

            case R.id.bn_logout:
                signOut();
                break;

            case R.id.bn_continue:
                bnContinue();
                break;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast toast = Toast.makeText(getBaseContext(), R.string.connection_failed, Toast.LENGTH_SHORT);
        toast.show();
    }

    // Get the result from the Google API, if logged in, update the UI by hide the google login button, and show the correct layout.
    private void handleResult(Task<GoogleSignInAccount> result)
    {
        try
        {
            GoogleSignInAccount account = result.getResult(ApiException.class);

            String name = account.getDisplayName();
            Data.loggedInUser = account.getEmail();
            Name.setText(name);
            Email.setText(Data.loggedInUser);

            updateUI(true);
            Intent intent = new Intent(SignInActivity.this, YearSelectionActivity.class);
            startActivity(intent);

        }catch(ApiException e)
        {
            updateUI(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQ_CODE)
        {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleResult(task);
        }
    }

    private void signIn()
    {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent intent = googleSignInClient.getSignInIntent();
        startActivityForResult(intent, REQ_CODE);
    }

    private void signOut()
    {
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUI(false);
            }
        });
    }
    private void updateUI(boolean isLogin)
    {
        if(isLogin)
        {
            Login_Layout.setVisibility(View.VISIBLE);
            SignIn.setVisibility(View.GONE);
        }else
        {
            Login_Layout.setVisibility(View.GONE);
            SignIn.setVisibility(View.VISIBLE);
        }
    }

    private void bnContinue()
    {
        Intent intent = new Intent(this, YearSelectionActivity.class);
        startActivity(intent);
    }

}
